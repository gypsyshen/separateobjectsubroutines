/*
Implementation of class PCSS.
*/

#include "Simple.h"
#include <string>
#include <fstream>

void Simple::Init() {

	loadPipeline();
}

void Simple::loadPipeline() {

	std::string VertexShaderCode = FileSource("Simple.vertexshader");
	char const * VertexSourcePointer = VertexShaderCode.c_str();

	std::string FragmentShaderCode_a = FileSource("Simple_a.fragmentshader");
	char const * FragmentSourcePointer_a = FragmentShaderCode_a.c_str();

	std::string FragmentShaderCode_b = FileSource("Simple_b.fragmentshader");
	char const * FragmentSourcePointer_b = FragmentShaderCode_b.c_str();

	vs_program = glCreateShaderProgramv(GL_VERTEX_SHADER, 1, &VertexSourcePointer);
	fs_program_a = glCreateShaderProgramv(GL_FRAGMENT_SHADER, 1, &FragmentSourcePointer_a);
	fs_program_b = glCreateShaderProgramv(GL_FRAGMENT_SHADER, 1, &FragmentSourcePointer_b);

	// pipelines
	pipelines = new GLuint[2];
	glGenProgramPipelines(2, pipelines);

	glBindProgramPipeline(pipelines[0]);
	glUseProgramStages(pipelines[0], GL_VERTEX_SHADER_BIT, vs_program);
	glUseProgramStages(pipelines[0], GL_FRAGMENT_SHADER_BIT, fs_program_a);

	glBindProgramPipeline(pipelines[1]);
	glUseProgramStages(pipelines[1], GL_VERTEX_SHADER_BIT, vs_program);
	glUseProgramStages(pipelines[1], GL_FRAGMENT_SHADER_BIT, fs_program_b);

	// glUniform* now heed the "active" shader program rather than glUseProgram
//	glActiveShaderProgram(pipeline, vs_program);
//	glUniform1f(fooLocation, 1.0f);

//	glActiveShaderProgram(pipeline, vs_program);
//	MVPID = glGetUniformLocation(vs_program, "MVP");

//	return pipeline;
}

void Simple::RenderToScreen(int window_size_w, int window_size_h,
	const glm::mat4 &MVP,
	const GLuint &vertexbuffer, const GLuint &uvbuffer, const GLuint &normalbuffer, GLuint &elementbuffer,
	const vector<unsigned short> &indices) const {

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, window_size_w, window_size_h); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glActiveShaderProgram(pipelines[0], vs_program);
	GLuint MVPID = glGetUniformLocation(vs_program, "MVP");
	glProgramUniformMatrix4fv(vs_program, MVPID, 1, GL_FALSE, &MVP[0][0]);

	glActiveShaderProgram(pipelines[0], fs_program_a);
	GLuint diffuseID = glGetUniformLocation(fs_program_a, "diffuse");
	float diffuse[3] = { 1, 1, 0 };
	glProgramUniform3fv(fs_program_a, diffuseID, 1, diffuse);

	// Bind pipeline
	glBindProgramPipeline(pipelines[0]);

	// Draw
	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

/*	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		1,                                // attribute
		2,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);

	// 3rd attribute buffer : normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glVertexAttribPointer(
		2,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);*/

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

	// Draw the triangles !
	glDrawElements(
		GL_TRIANGLES,      // mode
		indices.size(),    // count
		GL_UNSIGNED_SHORT, // type
		(void*)0           // element array buffer offset
		);

	glDisableVertexAttribArray(0);
//	glDisableVertexAttribArray(1);
//	glDisableVertexAttribArray(2);

	glBindProgramPipeline(0);
}