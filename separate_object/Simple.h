#ifndef Simple_H
#define Simple_H

#include "Header.h"

class Simple{

public:
	void Init();

	void RenderToScreen(int window_size_w, int window_size_h,
		const glm::mat4 &MVP,
		const GLuint &vertexbuffer, const GLuint &uvbuffer, const GLuint &normalbuffer, GLuint &elementbuffer,
		const vector<unsigned short> &indices) const;
	
private:
	void loadPipeline();

	// pipeline
	GLuint * pipelines;

	GLuint vs_program;
	GLuint fs_program_a;
	GLuint fs_program_b;
};

#endif