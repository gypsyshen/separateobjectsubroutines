/*
@HotKeys Left_Shift: switch the shadow methods.
@HotKeys Left_Alt: switch the display mode when using Hierarchy_mipmaps and Hierarchy_depths.
@HotKeys Space: store the screenshot.
@HotKeys Number_Keys: number of texels for filtering when using Hierarchy mipmaps and Hierarchy_depths.
@HotKeys '+', '-': control the light size.
@HotKeys 'W', 'S', 'A', 'D', 'Q', 'E': move the light.
@HotKeys Direction_Keys: move the camera.

@MouseKeys Left: light direction.
@MouseKeys Right: camera direction.
*/


#ifndef CONTROLS_HPP
#define CONTROLS_HPP


// save to png file
void saveScreenToImage();

// get shadow type
int getShadowType();
void setNumShadowMethods(int n);


// light size
float getLightSize();

// display mode: 0 - regular, 1 - low, 2 - high
int getDisplayMode();

// number of samples
int numberOfSamples();

// spot light
void computeSpotLightMatricesFromInputes();
glm::mat4 getLightProjectionMatrix();
glm::mat4 getLightViewMatrix();

glm::vec3 getLightRight();
glm::vec3 getLightPosition();
glm::vec3 getLightCenter();
glm::vec3 getLightUp();
glm::vec3 getLightInvDirection();
float getLightFoV();

// camera
void computeMatricesFromInputs();
glm::mat4 getViewMatrix();
glm::mat4 getProjectionMatrix();

#endif